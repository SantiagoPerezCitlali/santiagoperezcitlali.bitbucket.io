var Ajedrez = (function()
{
  var dom = (function()
  {
    var _crear_elemento = function(srt_etiqueta,obj_atributos,obj_propiedades)
    {
      var nuevo_elemento = document.createElement(srt_etiqueta);

      for(var llave in obj_atributos)
      {
        nuevo_elemento.setAttribute(llave,obj_atributos[llave]);
      }

      for(var llave in obj_propiedades)
      {
        nuevo_elemento[llave] = obj_propiedades[llave];
      }
      return nuevo_elemento;
    };
    return {
      crear_elemento: _crear_elemento
    };
  }());

  var _actualizar_tablero = function()
  {
    var tabla0 =document.getElementById("tablero");

    if(tabla0.firstChild)
    tabla0.removeChild(tabla0.firstChild);

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function()
    {
      if(this.readyState == 4 && this.status == 200)
      {
        var i, xmlDoc;
        xmlDoc = xmlhttp.response;
        var tabla = document.getElementById("tablero");
        var tcuerpo = document.createElement("tbody");
        console.log(xmlDoc);
        var dis = xmlDoc.split("\n");
        var e = dis[0].split("|");
        console.log(e)

        var bcolor=true;

        for (var i=0; i < 8; i += 1)
        {
          var r = dis[i+1].split("|");
          console.log(r);
          var tr = document.createElement("tr");

          for(var j=0; j< 8; j += 1)
          {
            if(j===0){
              var a = e[0];
            }
            if(j===1){
              var a =  e[1];
            }
            if(j===2){
              var a = e[2];
            }
            if(j===3){
              var a = e[3];
            }
            if(j===4){
              var a = e[4];
            }
            if(j===5){
              var a = e[5];
            }
            if(j===6){
              var a = e[6];
            }
            if(j===7){
              var a = e[7];
            }
            var bl = a + (8-i);
            var _td = dom.crear_elemento("td",{id:bl},{});
            var ficha = r[j];
             var texto = document.createTextNode(ficha);
            _td.appendChild(texto);
            tr.appendChild(_td);
          }
          tcuerpo.appendChild(tr);
        }
        tabla.appendChild(tcuerpo);
      }
    };
    xmlhttp.open("GET", "csv/tablero.csv", true);
    xmlhttp.send();
    };

  var _mostrar_tablero = function(){
    var cont = document.getElementById("opcion");
    var boton = document.createElement("button");
    boton.type = "button";
    boton.setAttribute("width","150");
    boton.setAttribute("heigth","150");
    boton.setAttribute("value","actualizar");
    boton.setAttribute("onClick","Ajedrez.actualizarTablero();");

    tablero.setAttribute("border", "1");
    tablero.style.width = "480px";
  	tablero.style.height = "480px";
  	tablero.setAttribute("cellSpacing","0");

    boton.innerHTML=" Actualizar tablero";
    cont.appendChild(boton);
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function()
    {
      if (this.readyState == 4 && this.status == 200)
      {
        var i, xmlDoc;
        xmlDoc = xmlhttp.response;
        var tabla = document.getElementById("tablero");
        var tcuerpo = document.createElement("tbody");
        console.log(xmlDoc);
        var dis = xmlDoc.split("\n");
        var e = dis[0].split("|");
        console.log(e)

        for (var i=0; i < 8; i += 1)
        {
          var r = dis[i+1].split("|");
          console.log(r);
          var tr = document.createElement("tr");

          for(var j=0; j< 8; j += 1)
          {
            if(j===0)
            {
              var a = e[0];
            }
            if(j===1)
            {
              var a =  e[1];
            }
            if(j===2)
            {
              var a = e[2];
            }
            if(j===3)
            {
              var a = e[3];
            }
            if(j===4)
            {
              var a = e[4];
            }
            if(j===5)
            {
              var a = e[5];
            }
            if(j===6)
            {
              var a = e[6];
            }
            if(j===7)
            {
              var a = e[7];
            }

            var bl = a + (8-i);
            var _td = dom.crear_elemento("td",{id:bl},{});
            var ficha = r[j];
            var texto = document.createTextNode(ficha);
            _td.appendChild(texto);
            tr.appendChild(_td);
          }
          tcuerpo.appendChild(tr);
        }
        tabla.appendChild(tcuerpo);
      }
    };
    xmlhttp.open("GET", "csv/tablero.csv", true);
    xmlhttp.send();
  };

  var _mover_pieza = function(x){
    var pr = x.de;
    var se = x.a;

    document.getElementById(se).textContent=document.getElementById(pr).textContent;
    document.getElementById(pr).textContent="∅";
  };
  return{
    "mostrarTablero": _mostrar_tablero,
    "moverPieza":_mover_pieza,
    "actualizarTablero": _actualizar_tablero
  }
})();
